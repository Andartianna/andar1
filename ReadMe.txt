-// ==UserScript==
// @name         Andar�s Lists
// @namespace    https://Andartianna@bitbucket.org/Andartianna/andar1.git
// @version      0.1
// @description  COTG Add-on
// @author       Andartianna
// @match        https://*.crownofthegods.com/World00.php
// @grant        none
// @updateURL    https://bitbucket.org/Andartianna/andar1.git
// @downloadURL  https://bitbucket.org/Andartianna/andar1.git
// ==/UserScript==
(function AndarScript() {
	console.log("updated 10.23.16");
    var loot=[0,350,9000,4250,14500,32000,59000,119000,200000,299000,445000]; //cavern loot per lvl
    var bossdef=[630,4100,25100,51000,127000,189000,252000,377000,564500,752000]; //bosses defense value
    var bossdefw=[430,2550,17500,34000,84000,126000,171000,251000,376000,502000]; // bosses defense value for weakness type
    var numbs=[0,0,0];
    var ttloot=[0,0,10,20,10,10,5,0,15,20,15,10,0,0,1000,1500,3000]; //troops loot capacity
    var ttattack=[10,50,30,10,25,50,70,10,40,60,90,120,50,150,3000,1200,12000]; //troops attack value
    var ttspeed=[0,30,20,20,20,20,20,8,10,10,10,10,30,30,5,5,5];
    var iscav=[0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0]; //which troop number is cav
    var isinf=[1,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0]; //which troop number is inf
    var ismgc=[0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0]; //which troop number is magic
    var isart=[0,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1]; //which troop number is artillery
    var ttres=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
    var ttspeedres=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
    var resbonus=[0,0.01,0.03,0.06,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5]; // res bonus to attack power per res rank
    var ttname=["Guards","Ballistas","Rangers","Triari","Priestess","Vanquishers","Sorcerers","Scouts","Arbalists","Praetors","Horsemans","Druids","Rams","Scorpions","Galleys","Stingers","Warships"];
    var layoutsl=[""];
    var layoutsw=[""];
    var tpicdiv=["","","wcitrpsd warcrangtooltip ranger20 tooltipstered","wcitrpsd warctriatooltip triari20 tooltipstered","wcitrpsd warclegitooltip priest20 tooltipstered","wcitrpsd warcvanqtooltip vanq20 tooltipstered","wcitrpsd warcsorctooltip sorc20 tooltipstered"];
    tpicdiv[8]="wcitrpsd warcarbtooltip arbal20 tooltipstered";tpicdiv[9]="wcitrpsd warcpraetooltip praet20 tooltipstered"; tpicdiv[10]="wcitrpsd warchorstooltip horsem20 tooltipstered";tpicdiv[11]="wcitrpsd warcdruitooltip druid20 tooltipstered";
    tpicdiv[14]="wcitrpsd warcgalltooltip galley20 tooltipstered";tpicdiv[15]="wcitrpsd warcstintooltip sting20 tooltipstered";tpicdiv[16]="wcitrpsd warcwstooltip wship20 tooltipstered";
    var buildings={name: ["forester","cottage","storehouse","quarry","hideaway","farmhouse","cityguardhouse","barracks","mine","trainingground","marketplace","townhouse","sawmill","stable","stonemason","mage_tower","windmill","temple","smelter","blacksmith",
                       "castle","port","port","port","shipyard","shipyard","shipyard","townhall"],
               bid: [448,446,464,461,479,447,504,445,465,483,449,481,460,466,462,500,463,482,477,502,"467",488,489,490,491,496,498,455]};
    var vexfaith=0,ibriafaith=0,ylannafaith=0,naerafaith=0,cyndrosfaith=0,domdisfaith=0; //alliance faiths
    var cdata; //city data return
    var wdata; //world data
    var pdata; //player data
    var city={cid:0,x:0,y:0,th:[0],cont:0}; //current city data
    var bosses={name:["Cyclops","Andar's Colosseum Challenge","Dragon","Romulus and Remus","Gorgon","GM Gordy","Triton"],
                pic:["cyclops32 mauto bostooltip tooltipstered","andar32 mauto bostooltip tooltipstered","dragon32 mauto bostooltip tooltipstered","romrem32 mauto bostooltip tooltipstered","gorgon32 mauto bostooltip tooltipstered","gmgordy32 mauto bostooltip tooltipstered","triton32 mauto bostooltip tooltipstered"]};
    var bossinfo={x:[],y:[],lvl:[],data:[],name:[],cont:[],distance:[]};
    var key="_`abcdefgh";
    var remarksl=[""];
    var remarksw=[""];
    var troopcounw=[[]];
    var troopcounl=[[]];
    var resw=[[]];
    var resl=[[]];
    var notesl=[""];
    var notesw=[""];
    var emptyspots=",.;:#-T";
    var beentoworld=false;


    (function(open) {
        XMLHttpRequest.prototype.open = function() {
            this.addEventListener("readystatechange", function() {
                if(this.readyState==4) {
                    var url=this.responseURL;
                    if (url.indexOf('gC.php')!=-1) {
                        cdata=JSON.parse(this.response);
                        city.cid=cdata.cid;
                        city.th=cdata.th;
                        city.x=Number(city.cid % 65536);
                        city.y=Number((city.cid-city.x)/65536);
                        city.cont=Number(Math.floor(city.x/100)+10*Math.floor(city.y/100));
                        city.mo=cdata.mo
                        makebuildcount();
                        /*if (pdata.pid==1197) {
                            $("#bossbox").html("");
                            $("#idleunits").html("");
                            },
                                         success: function(data) {
                                             wdata=JSON.parse(data);
                                             console.log("Andar");
                                             getbossinfo();
                                         }
                                        });
                        }*/
                    }
                    if (url.indexOf('gWrd.php')!=-1) {
                        wdata=JSON.parse(this.response);
                        //console.log("not Andar");
                        getbossinfo();
                        beentoworld=true;


                    }
                }
            }, false);
            open.apply(this, arguments);
        };
    })(XMLHttpRequest.prototype.open);
    


    
    
    function errormsgBR(a, b) {
        $(a).show();
        $(b).animate({ opacity: 1, bottom: "+10px" }, 'slow');
        errormsgBRhide(a, b);
    }
    
    function errormsgBRhide(a, b) {
        setTimeout(function(){ 
            $(b).animate({ opacity: 0, bottom: "-10px" }, 'slow');
            $(a).fadeOut("slow");
        }, 5000);
        setTimeout(function(){ 
            $(a).remove();
        }, 6000);
    }
    
    var errmBR=0;
    var message="Impossible action, one needs more troops at least ";
    //console.log(message);
    
    function errorgo(j) {
        var errormsgs;
        errmBR = errmBR+1;
        var b = 'errBR' +errmBR;
        var c = '#' +b;
        var d = '#' +b+ ' div';
        errormsgs = '<tr ID = "' +b+ '"><td><div class = "errBR">' +j+ '<div></td></tr>';
        $("#errorBRpopup").append(errormsgs);
        errormsgBR(c, d);
    }




    String.prototype.replaceAt=function(index, char) {
        var a = this.split("");
        a[index] = char;
        return a.join("");
    }
    String.prototype.decrypt=function() {
    var a=this;
    for (var i in a) {
        for (var j in key) {
            if (a.charAt(i)==key.charAt(j)) {
                a=a.replaceAt(i,j);
            }
        }
    }
    return a;
    };


    function getbossinfo() {
        var temp;
        bossinfo={x:[],y:[],lvl:[],data:[],name:[],cont:[],distance:[],cid:[]};
        for (var i in wdata.a) {
            temp=wdata.a[i];
            if (temp.charAt(0,1)=="1") {
                var tempx=Number(temp.substring(1,4))-100;
                var tempy=Number(temp.substring(4,7))-100;
                var templvl=Number(temp.substring(8,10))-10;
                //var distance=(Math.sqrt((tempx-city.x)*(tempx-city.x)+(tempy-city.y)*(tempy-city.y)));
                var cid=tempy*65536+tempx;
                bossinfo.x.push(tempx);
                bossinfo.y.push(tempy);
                bossinfo.lvl.push(templvl);
                bossinfo.cont.push(Number(Math.floor(tempx/100)+10*Math.floor(tempy/100)));
                //bossinfo.distance.push(distance);
                bossinfo.data.push(temp);
                bossinfo.cid.push(cid);
            }
        }
        /*var i=0; var end=bossinfo.cid.length;
        function loop() {
            var templink="<div id='tempcl' class='coordblink shcitt' data='"+bossinfo.cid[i]+"' style='text-align: center;'>"+bossinfo.x[i]+":"+bossinfo.y[i]+"</div>";
            $("#bossbox").html(templink);
            $("#tempcl").click();
            setTimeout(function(){
                var tname=$("#dungtypespot").text();
                bossinfo.name.push(tname);
            },290);
            i++;
            if (i<end) {
                setTimeout( loop, 300 );
            } else {
                console.log(bossinfo);
                openbosswin();
            }
        }
        loop();*/
        //openbosswin();
    }    
    // boss raiding assistant 
    $(document).ready(function() {
        jQuery.ajax({url: 'includes/pD.php',type: 'POST',aysnc:false,
                     success: function(data) {
                         pdata=JSON.parse(data);
                         //console.log(pdata);
                     }
                    });
        var cid=$("#cityDropdownMenu").val();
        var dat={a:cid};
        jQuery.ajax({url: 'includes/gC.php',type: 'POST',aysnc:false,data:dat});
        var returnAllbut="<button id='returnAllb' style='right: 36%; margin-top: 55px;width: 150px;height: 30px !important; font-size: 12px !important; position: absolute;' class='regButton redb'>Return All</button>";
        var raidbossbut="<button id='raidbossGo' style='left: 63%;margin-left: 10px;margin-top: 15px;width: 150px;height: 30px !important; font-size: 12px !important; position: absolute;' class='regButton redb'>Locate Bosses</button>";
        var bosstab="<li id='bosshuntab' class='ui-state-default ui-corner-top' role='tab' tabindex='-1' aria-controls='warBossmanager'";
        bosstab+="aria-labeledby='ui-id-20' aria-selected='false' aria-expanded='false'>";
        bosstab+="<a href='#warBossmanager' class='ui-tabs-anchor' role='presentation' tabindex='-1' id='ui-id-20'>Find Bosses</a></li>";
        var bosstabbody="<div id='warBossmanager' aria-labeledby='ui-id-20' class='ui-tabs-panel ui-widget-content ui-corner-bottom' ";
        bosstabbody+=" role='tabpanel' aria-hidden='true' style='display: none;'><div id='fpdcdiv3' class='redheading' style='margin-left: 2%;' >Andar's Boss Raiding Assistant:</div>";
        bosstabbody+="<div id='bossbox' class='beigemenutable scroll-pane' style='width: 96%; height: 85%; margin-left: 2%;'></div>";
        bosstabbody+="<div id='idletroops'></div></div>";
        
        var tabs = $( "#warcouncTabs" ).tabs();
        var ul = tabs.find( "ul" );
        $( bosstab ).appendTo( ul );
        tabs.tabs( "refresh" );
        
        $('#warCidlemanager').after(bosstabbody);
        $('#bosshuntab').click(function() {
            if (beentoworld)
            {
                openbosswin();
            } else {
                alert("Press World Button");
            }
        });
        $("#warCounc").append(returnAllbut);
        $("#loccavwarconGo").css("right","65%");
        $("#idluniwarconGo").css("left","34%");
        $("#idluniwarconGo").after(raidbossbut);
        $('#returnAllb').click(function() {
            jQuery.ajax({url: 'includes/gIDl.php',type: 'POST',aysnc:false,
                         success: function(data) {
                             var thdata=JSON.parse(data);
                             $("#returnAll").remove();
                             openreturnwin(thdata);
                         }
                        });
        });
        $('#raidbossGo').click(function() {
            if (beentoworld)
            {
                $("#warcouncbox").show();
                tabs.tabs( "option", "active", 2 );
                $("#bosshuntab").click();
            } else {
                alert("Press World Button");
            }
        });
    });


    function openreturnwin(data) {
        //console.log(data);
        var selectcont=$("#idleCsel").clone(false).attr({id:"selcr",style:"width:40%;height:28px;font-size:11;border-radius:6px;margin:7px"});
        var returnwin="<div id='returnAll' style='width:300px;height:320px;background-color: #E2CBAC;-moz-border-radius: 10px;-webkit-border-radius: 10px;border-radius: 10px;border: 4px ridge #DAA520;position:absolute;right:100px;top:100px; z-index:1000000;'><div class=\"popUpBar\"> <span class=\"ppspan\">Return all troops in all cities</span>";
        returnwin+="<button id=\"AndarZ\" onclick=\"$('#returnAll').remove();\" class=\"xbutton redb\"><div id=\"xbuttondiv\"><div><div id=\"centxbuttondiv\"></div></div></div></button></div><div id='returnbody' class=\"popUpWindow\">"
        returnwin+="<div id='rethead' class='redheading' style='margin-left: 2%;'>Open Return window from a raid before running this</div></div></div>";
        var selecttype="<select id='selType' class='redsel' style='width:50%;height:28px;font-size:11;border-radius:6px;margin:7px'><option value='1'>Offence and Defense</option><option value='2'>Offence</option><option value='3'>Defense</option></select><br>";
        var selectret=$("#raidrettimesela").clone(false).attr({id:"returnSel",style:"width:40%;height:28px;font-size:11;border-radius:6px;margin:7px"});
        var selecttime="<br><div id='timeblock' style='height:100px; width 95%'><div id='timesel' style='display: none;'><span style='text-align:left;font-weight:800;margin-left:2%;'>Input latest return time:</span><br><table style='width:80%;margin-left:10px'><thead><tr style='text-align:center;'><td>Hr</td><td>Min</td><td>Sec</td><td colspan='2'>Date</td></tr></thead><tbody>";
        selecttime+="<tr><td><input id='returnHr' type='number' style='width: 35px;height: 22px;font-size: 10px;padding: none !important;color: #000;' value='00'></td><td><input id='returnMin' style='width: 35px;height: 22px;font-size: 10px;padding: none !important;color: #000;' type='number' value='00'></td>";
        selecttime+="<td><input style='width: 35px;height: 22px;font-size: 10px;padding: none !important;color: #000;' id='returnSec' type='number' value='00'></td><td colspan='2'><input style='width:90px;' id='returnDat' type='text' value='00/00/0000'></td></tr></tbody></table></div></div>";
        var returnAllgo="<button id='returnAllGo' style='margin-left:30%; width: 35%;height: 30px !important; font-size: 12px !important; position: static !important;' class='regButton redb'>Start Return All</button><br>";
        var nextcity="<button id='nextCity' style='display: none;margin-left:10%; width: 35%;height: 30px !important; font-size: 12px !important; position: static !important;' class='regButton redb'>Next City</button>";
        var returntroops="<button id='returnTroops' style='display: none;margin-left:10%; width: 35%;height: 30px !important; font-size: 12px !important; position: static !important;' class='regButton redb'>Return troops</button>";
        var selectlist=$("#organiser").clone(false).attr({id:"selClist",style:"width:40%;height:28px;font-size:11;border-radius:6px;margin:7px"});
        $("body").append(returnwin);
        $("#returnAll").draggable({ handle: ".popUpBar" , containment: "window", scroll: false});
        $("#returnbody").html(selectcont);
        //$("#selcr").attr("style","width:40%;height:28px;font-size:11;border-radius:6px;margin:7px");
        $("#selcr").after(selecttype);
        $("#selType").after(selectret);
        $("#returnSel").after(selectlist);
        $("#selClist").after(selecttime);
        $(function() {
        $( "#returnDat" ).datepicker();
        });
        //$("#timesel").hide();
        $("#returnbody").append(returnAllgo);
        $("#returnAllGo").after(nextcity);
        $("#nextCity").after(returntroops);
        //$("#nextCity").hide();
        $("#returnSel").change(function() {
            if ($("#returnSel").val()==3) {
                $("#timesel").show();
            } else {
                $("#timesel").hide();
            }
        });
        var j,end,bb,cc,aa;
         var returncities={cid:[],cont:[]};
        $("#returnAllGo").click(function() {
            if ($("#selClist").val()=="all") {
                for (var i in data) {
                    var cont=data[i].c.co;
                    if ($("#selcr").val()==56) {
                        if($("#selType").val()==1) {
                            returncities.cid.push(data[i].i);
                            returncities.cont.push(cont);
                        }
                        if($("#selType").val()==2) {
                            if (data[i].tr.indexOf(5)>-1 || data[i].tr.indexOf(6)>-1 || data[i].tr.indexOf(10)>-1 || data[i].tr.indexOf(11)>-1 || data[i].tr.indexOf(12)>-1 || data[i].tr.indexOf(13)>-1 || data[i].tr.indexOf(14)>-1 || data[i].tr.indexOf(16)>-1) {
                                returncities.cid.push(data[i].i);
                                returncities.cont.push(cont);
                            }
                        }
                        if($("#selType").val()==3) {
                            if (data[i].tr.indexOf(1)>-1 || data[i].tr.indexOf(2)>-1 || data[i].tr.indexOf(3)>-1 || data[i].tr.indexOf(4)>-1 || data[i].tr.indexOf(8)>-1 || data[i].tr.indexOf(9)>-1 || data[i].tr.indexOf(15)>-1) {
                                returncities.cid.push(data[i].i);
                                returncities.cont.push(cont);
                            }
                        }
                    }
                    if (cont==Number($("#selcr").val())) {
                        if($("#selType").val()==1) {
                            returncities.cid.push(data[i].i);
                            returncities.cont.push(cont);
                        }
                        if($("#selType").val()==2) {
                            if (data[i].tr.indexOf(5)>-1 || data[i].tr.indexOf(6)>-1 || data[i].tr.indexOf(10)>-1 || data[i].tr.indexOf(11)>-1 || data[i].tr.indexOf(12)>-1 || data[i].tr.indexOf(13)>-1 || data[i].tr.indexOf(14)>-1 || data[i].tr.indexOf(16)>-1) {
                                returncities.cid.push(data[i].i);
                                returncities.cont.push(cont);
                            }
                        }
                        if($("#selType").val()==3) {
                            if (data[i].tr.indexOf(1)>-1 || data[i].tr.indexOf(2)>-1 || data[i].tr.indexOf(3)>-1 || data[i].tr.indexOf(4)>-1 || data[i].tr.indexOf(8)>-1 || data[i].tr.indexOf(9)>-1 || data[i].tr.indexOf(15)>-1) {
                                returncities.cid.push(data[i].i);
                                returncities.cont.push(cont);
                            }
                        }
                    }
                }
                //}
            } else {
                $.each(pdata.clc, function(key, value) {
                    if (key==$("#selClist").val()) {
                        returncities.cid=value;
                    }
                });
            }
            //console.log(returncities);
            $("#organiser").val("all").change();
            //$("#outgoingPopUpBox").open();
            bb=$("#returnSel").val();
            if (bb==3) {
                cc=$("#returnDat").val()+" "+$("#returnHr").val()+":"+$("#returnMin").val()+":"+$("#returnSec").val();
            } else {cc=0}
            j=0; end=returncities.cid.length;
            aa=returncities.cid[j];
            //console.log(aa);
            $("#cityDropdownMenu").val(aa).change();
            $("#returnTroops").show();
            $("#nextCity").show();
            //$("#returnAllGo").attr("id","nextCity").html("Next City");
            $("#returnAllGo").hide();
        });
        $("#returnTroops").click(function() {
            $("#raidrettimesela").val(bb);
            $("#raidrettimeselinp").val(cc);
            $("#doneOGAll").click();
        });
        $("#nextCity").click(function() {
            j++;
            console.log(j,end);
            if (j==end)  {
                alert("Return all complete");
                $("#returnAll").remove();
                         }
            else {
                aa=returncities.cid[j];
                console.log(aa,j,end);
                $("#cityDropdownMenu").val(aa).change();
            }
        });
    }


    function openbosswin() {
        //console.log(bossinfo);
        var bosslist={name:[],x:[],y:[],lvl:[],distance:[],cid:[],time:[],cont:[]};
        for (var i in bossinfo.cont) {
            var distance=(Math.sqrt((bossinfo.x[i]-city.x)*(bossinfo.x[i]-city.x)+(bossinfo.y[i]-city.y)*(bossinfo.y[i]-city.y)));
            //if ((bossinfo.cont[i]==city.cont) && ((bossinfo.name[i]!="Triton")) && (bossinfo.data[i].charAt(0,1)==1)) {            
            if (city.th[2] || city.th[3] || city.th[4]|| city.th[5]|| city.th[6]|| city.th[8]|| city.th[9]|| city.th[10]|| city.th[11]) {
                if (bossinfo.cont[i]==city.cont) {
                    if (city.th[2] || city.th[3] || city.th[4]|| city.th[5]|| city.th[6]) {
                        var minutes=distance*ttspeed[2]/ttspeedres[2];
                        var time=Math.floor(minutes/60)+"h "+Math.floor(minutes % 60)+"m";
                    }
                    if (city.th[8] || city.th[9] || city.th[10]|| city.th[11]) {
                        var minutes=distance*ttspeed[8]/ttspeedres[8];
                        var time=Math.floor(minutes/60)+"h "+Math.floor(minutes % 60)+"m";
                    }
                    //bosslist.name.push(bossinfo.name[i]);
                    bosslist.x.push(bossinfo.x[i]);
                    bosslist.y.push(bossinfo.y[i]);
                    bosslist.cid.push(Number(bossinfo.y[i]*65536+bossinfo.x[i]));
                    bosslist.lvl.push(bossinfo.lvl[i]);
                    bosslist.distance.push(Math.floor(distance));
                    bosslist.time.push(time);
                    bosslist.cont.push(bossinfo.cont[i]);
                }
            }
            if (distance<180) {
                if (city.th[14] || city.th[15] || city.th[16]) {
                    var minutes=distance*ttspeed[14]/ttspeedres[14];
                    var time=Math.floor(minutes/60)+"h "+Math.floor(minutes % 60)+"m";
                    //bosslist.name.push(bossinfo.name[i]);
                    bosslist.x.push(bossinfo.x[i]);
                    bosslist.y.push(bossinfo.y[i]);
                    bosslist.cid.push(Number(bossinfo.y[i]*65536+bossinfo.x[i]));
                    bosslist.lvl.push(bossinfo.lvl[i]);
                    bosslist.distance.push(Math.floor(distance));
                    bosslist.time.push(time);
                    bosslist.cont.push(bossinfo.cont[i]);
                }
            }
        }
        //console.log(bosslist);
        //var bosswin="<table id='bosstable' class='beigetablescrollp sortable'><thead><tr><th></th><th>Type</th><th>Level</th><th>Coordinates</th><th>Travel Time</th><th id='hdistance'>Distance</th></tr></thead>";
        var bosswin="<table id='bosstable' class='beigetablescrollp sortable'><thead><tr><th>Coordinates</th><th>Level</th><th>Continent</th><th>Travel Time</th><th id='hdistance'>Distance</th></tr></thead>";
        bosswin+="<tbody>";
        for (var i in bosslist.x) {
            var j=bosses.name.indexOf(bosslist.name[i]);
            /*bosswin+="<tr id='bossline"+bosslist.cid[i]+"' class='dunginf'><td><button id='"+bosslist.cid[i]+"' class='greenb'>Hit Boss</button></td>";
            bosswin+="<td style='text-align: center;'><div class='"+bosses.pic[j]+"'></div></td><td style='text-align: center;'>"+bosslist.lvl[i]+"</td>";
            bosswin+="<td id='cl"+bosslist.cid[i]+"' class='coordblink shcitt' data='"+bosslist.cid[i]+"' style='text-align: center;'>"+bosslist.x[i]+":"+bosslist.y[i]+"</td><td style='text-align: center;'>"+bosslist.time[i]+"</td><td style='text-align: center;'>"+bosslist.distance[i]+"</td></tr>";*/
            bosswin+="<tr id='bossline"+bosslist.cid[i]+"' class='dunginf'><td id='cl"+bosslist.cid[i]+"' class='coordblink shcitt' data='"+bosslist.cid[i]+"' style='text-align: center;'>"+bosslist.x[i]+":"+bosslist.y[i]+"</td>";
            bosswin+="<td style='text-align: center;font-weight: bold;'>"+bosslist.lvl[i]+"</td><td style='text-align: center;'>"+bosslist.cont[i]+"</td>";
            bosswin+="<td style='text-align: center;'>"+bosslist.time[i]+"</td><td style='text-align: center;'>"+bosslist.distance[i]+"</td></tr>";
        }
        bosswin+="</tbody></table></div>";
        var idle="<table id='idleunits' class='beigetablescrollp'><tbody><tr><td style='text-align: center;'><span>Idle troops:</span></td>";
        for (var i in city.th) {
            var notallow=[0,1,7,12,13];
            if (notallow.indexOf(i)==-1) {
                if (city.th[i]>0) {
                    idle+="<td><div class='"+tpicdiv[i]+"' style='text-align: right;'></div></td><td style='text-align: left;'><span id='thbr"+i+"' style='text-align: left;'>"+city.th[i]+"</span></td>";
                }
            }
        }
        idle+="</tbody></table>";
        $("#bossbox").html(bosswin);
        $("#idletroops").html(idle);
        var newTableObject = document.getElementById('bosstable');
        sorttable.makeSortable(newTableObject);
        //console.log(bosslist);
        setTimeout(function(){
            $("#hdistance").trigger("click");
            setTimeout(function(){$("#hdistance").trigger("click");},100);
        },100);
        for (var i in bosslist.x) {
            $("#cl"+bosslist.cid[i]).click(function() {
                setTimeout(function(){
                    $("#raidDungGo").trigger("click");
                },500);
            });
        }
    }




    //fill queue button
    $(document).ready(function() {
        var fillbut='<button id="fillque" class="redb tooltipstered" style="height:18px; width:40px; margin-left:7px; margin-top:5px ; border-radius:4px ; font-size: 10px !important; padding: 0px;">Fill</button>';
        $('#sortbut').after(fillbut);
        $('#fillque').click(function() {
            var cid=cdata.cid;
            event.stopPropagation();
            var bB = $.post('/overview/fillq.php', { a: cid });
        });
    });
    // auto demolish button
    $(document).ready(function() {
        var autodemoon=0;
        var autodemobut='<button id="autodemo" style="margin-left:2%;width:22%;border: none !important;" class="regButton couonpos">Quick Demo</button>';
        setTimeout(1000);
        $('#quickRefineGo').width('23%');
        $('#quickBuildMode').width('22%');
        $('#cityNotesGo').width('21%');
        $('#quickBuildMode').after(autodemobut);
        $('#autodemo').click(function() {
            if (autodemoon==0) {
                autodemoon=1;
                //errorgo("Quick demolish on");
                $("#autodemo").removeClass('couonpos').addClass('couoffpos');
                //console.log(autodemoon);
            } else {
                autodemoon=0;
                //errorgo("Quick demolish off");
                $("#autodemo").removeClass('couoffpos').addClass('couonpos');
                //console.log(autodemoon);
            }           
        });
        
        $('#arrowNextDiv').click(function() {
            autodemoon=0;
            $("#autodemo").removeClass('couoffpos').addClass('couonpos');
        });
        
        $('#arrowPrevDiv').click(function() {
            autodemoon=0;
            $("#autodemo").removeClass('couoffpos').addClass('couonpos');
        });
        
        $('#ddctd').click(function() {
            autodemoon=0;
            $("#autodemo").removeClass('couoffpos').addClass('couonpos');
        });
        $('#quickBuildMode').click(function() {
            autodemoon=0;
            $("#autodemo").removeClass('couoffpos').addClass('couonpos');
        });




        $("#city_map").click(function() {
            //console.log(autodemoon);
            if (autodemoon==1) {
                $('#buildingDemolishButton').click();
            }
        });
    });
                      
    // setting layouts
    $(document).ready(function() {
        $('#citynotes').height('290px');
        $('#citynotes').width('495px');
        var layoutopttab="<li id='layoutopt' class='ui-state-default ui-corner-top' role='tab' tabindex='-1' aria-controls='layoutoptBody'";
        layoutopttab+="aria-labeledby='ui-id-60' aria-selected='false' aria-expanded='false'>";
        layoutopttab+="<a href='#layoutoptBody' class='ui-tabs-anchor' role='presentation' tabindex='-1' id='ui-id-60'>Layout Options</a></li>";
        var layoutoptbody="<div id='layoutoptBody' aria-labeledby='ui-id-60' class='ui-tabs-panel ui-widget-content ui-corner-bottom' ";
        layoutoptbody+=" role='tabpanel' aria-hidden='true' style='display: none;'><table><tbody><tr><td><input id='addnotes' class='clsubopti' type='checkbox'> Add Notes</td>";
        layoutoptbody+="<td><input id='addtroops' class='clsubopti' type='checkbox'> Add Troops</td></tr><tr><td><input id='addtowers' class='clsubopti' type='checkbox'> Add Towers</td><td><input id='addbuildings' class='clsubopti' type='checkbox'> Upgrade Cabins</td>";
        layoutoptbody+="<td> Cabin Lvl: <input id='cablev' type='number' style='width:22px;'></td></tr><tr><td><input id='addwalls' class='clsubopti' type='checkbox'> Add Walls</td>";
        layoutoptbody+="<td><input id='addhub' class='clsubopti' type='checkbox'> Use Nearest Hub</td></tr><tr><td>Select Hubs list: </td><td id='selhublist' colspan='2'></td></tr></tbody></table>";
        layoutoptbody+="<table><tbody><tr><td><input id='addres' class='clsubopti' type='checkbox'> Add Resources:</td><td id='buttd' colspan='2'></td></tr><tr><td>wood<input id='woodin' type='number' style='width:100px;'></td><td>stones<input id='stonein' type='number' style='width:100px;'></td>";
        layoutoptbody+="<td>iron<input id='ironin' type='number' style='width:100px;'></td><td>food<input id='foodin' type='number' style='width:100px;'></td></tr>";
        layoutoptbody+="</tbody></table></div>";
        var layoptbut="<button id='layoptBut' class='regButton redb' style='width:100px;'>Apply</button>";
        var tabs = $( "#CNtabs" ).tabs();
        var ul = tabs.find( "ul" );
        $( layoutopttab ).appendTo( ul );
        tabs.tabs( "refresh" );
        $("#CNtabs").append(layoutoptbody);
        $("#buttd").append(layoptbut);
        $("#layoptBut").click(function() {
            localStorage.setItem('woodin',$("#woodin").val());
            localStorage.setItem('foodin',$("#foodin").val());
            localStorage.setItem('ironin',$("#ironin").val());
            localStorage.setItem('stonein',$("#stonein").val());
            localStorage.setItem('cablev',$("#cablev").val());
        });
        if (localStorage.getItem('cablev')) {
            $("#cablev").val(localStorage.getItem('cablev'));
        }
        if (localStorage.getItem('woodin')) {
            $("#woodin").val(localStorage.getItem('woodin'));
        }
        if (localStorage.getItem('stonein')) {
            $("#stonein").val(localStorage.getItem('stonein'));
        }
        if (localStorage.getItem('ironin')) {
            $("#ironin").val(localStorage.getItem('ironin'));
        }
        if (localStorage.getItem('foodin')) {
            $("#foodin").val(localStorage.getItem('foodin'));
        }
        if (localStorage.getItem('atroops')) {
            if (localStorage.getItem('atroops')==1) {
                $("#addtroops").prop( "checked", true );
            }
        }
        if (localStorage.getItem('ares')) {
            if (localStorage.getItem('ares')==1) {
                $("#addres").prop( "checked", true );
            }
        }
        if (localStorage.getItem('abuildings')) {
            if (localStorage.getItem('abuildings')==1) {
                $("#addbuildings").prop( "checked", true );
            }
        }
        if (localStorage.getItem('anotes')) {
            if (localStorage.getItem('anotes')==1) {
                $("#addnotes").prop( "checked", true );
            }
        }
        if (localStorage.getItem('awalls')) {
            if (localStorage.getItem('awalls')==1) {
                $("#addwalls").prop( "checked", true );
            }
        }if (localStorage.getItem('atowers')) {
            if (localStorage.getItem('atowers')==1) {
                $("#addtowers").prop( "checked", true );
            }
        }
        if (localStorage.getItem('ahub')) {
            if (localStorage.getItem('ahub')==1) {
                $("#addhub").prop( "checked", true );
            }
        }
        $("#addnotes").change(function() {
            if ($("#addnotes").prop( "checked")==true) {
                localStorage.setItem('anotes',1);
            } else {localStorage.setItem('anotes',0);}
        });
        $("#addres").change(function() {
            if ($("#addres").prop( "checked")==true) {
                localStorage.setItem('ares',1);
            } else {localStorage.setItem('ares',0);}
        });
        $("#addtroops").change(function() {
            if ($("#addtroops").prop( "checked")==true) {
                localStorage.setItem('atroops',1);
            } else {localStorage.setItem('atroops',0);}
        });
        $("#addbuildings").change(function() {
            if ($("#addbuildings").prop( "checked")==true) {
                localStorage.setItem('abuildings',1);
            } else {localStorage.setItem('abuildings',0);}
        });
        $("#addwalls").change(function() {
            if ($("#addwalls").prop( "checked")==true) {
                localStorage.setItem('awalls',1);
            } else {localStorage.setItem('awalls',0);}
        });
        $("#addtowers").change(function() {
            if ($("#addtowers").prop( "checked")==true) {
                localStorage.setItem('atowers',1);
            } else {localStorage.setItem('atowers',0);}
        });
        $("#addhub").change(function() {
            if ($("#addhub").prop( "checked")==true) {
                localStorage.setItem('ahub',1);
            } else {localStorage.setItem('ahub',0);}
        });
        
        $("#cityNotesGo").click(function() {
            $("#selHub").remove();
            var selhub=$("#organiser").clone(false).attr({id:"selHub",style:"width:40%;height:28px;font-size:11;border-radius:6px;margin:7px"});
            $("#selhublist").append(selhub);
            if (localStorage.getItem('hublist')) {
                $("#selHub").val(localStorage.getItem('hublist')).change();
            }
            $("#selHub").change(function() {
                localStorage.setItem('hublist',$("#selHub").val());
            });
            $('#andarlayoutl').remove();
            $('#andarlayoutw').remove();
            setTimeout(function(){
                var currentlayout=$('#currentLOtextarea').text();
                for (var i=20; i<currentlayout.length-20;i++) {
                    var tmpchar=currentlayout.charAt(i);
                    //console.log(tmpchar);
                    var cmp=new RegExp(tmpchar);
                    if (!(cmp.test(emptyspots))) {
                        //console.log(String(cmp));
                        currentlayout=currentlayout.replaceAt(i,"-");
                    }
                }
                //console.log(currentlayout);
                var selectbuttsw='<select id="andarlayoutw" style="font-size: 10px !important;margin-top:1%;margin-left:2%;width:45%;" class="regButton redb"><option value="0">Select water layout</option>';
                var ww=1;
                selectbuttsw+='<option value="'+ww+'">2 sec rang/galley</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##BGBGB##-----##----##GBGBGBG##----##----#BGBGBGBGB#----##----#BGBGBGBGB#---H#######BGBGTGBGB#######----#BGBGBGBGB#JSPX##----#BGBGBGBGB#----##----##GBGBGBG##G---##-----##BGGGB##BBBBG##------#######BBVVBB##---------#--GBV##VB##---------#--GBV###V###--------#---BBV#######-------#----BBV########################");
                remarksw.push("rangers/triari/galley"); notesw.push("166600 inf and 334 galley @ 10 days");
                troopcounw.push([0,0,83300,83300,0,0,0,0,0,0,0,0,0,0,334,0,0]);
                resw.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">3 sec priestess/galley</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##BZBZB##-----##----##ZBZBZBZ##----##----#BZBZBZBZB#----##----#BZBZBZBZB#---H#######BZBZTZBZB#######----#BZBZBZBZB#JSPX##----#BZBZBZBZB#----##----##ZBZBZBZ##-Z--##-----##BZZZB##BBBBZ##------#######BBVVBB##---------#---BV##VB##---------#--ZBV###V###--------#---BBV#######-------#---ZBBV########################");
                remarksw.push("priestess/galley"); notesw.push("166600 inf and 334 galley @ 11 days");
                troopcounw.push([0,0,0,0,166600,0,0,0,0,0,0,0,0,0,334,0,0]);
                resw.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">2 sec vanq/galley+senator</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##BGBGB##-----##----##BBGBGBB##----##----#BGBGBGBGB#----##----#BGBGBGBGB#---H#######BGBGTGBGB#######----#BGBGBGBGB#JSPX##----#BGBGBGBGB#----##----##BBGBGBB##---B##-----##BGBGB##BBBBZ##------#######BBVVBB##---------#---BV##VB##---------#---BV###V###--------#---BBV#######-------#--BBBBV########################");
                remarksw.push("vanq/galley+senator"); notesw.push("193300 inf and 387 galley @ 10 days");
                troopcounw.push([0,0,0,0,0,193300,0,0,0,0,0,0,0,0,387,0,0]);
                resw.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">5 sec horses/galley</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##BEBEB##-----##----##EBEBEBE##----##----#BEBEBEBEB#----##----#BEBEBEBEB#---H#######BEBETEBEB#######----#BEBEBEBEB#JSPX##----#BEBEBEBEB#-M--##----##EBEBEBB##----##-----##BEBEB##BBBB-##------#######BBVVBB##---------#---BV##VB##---------#---BV###V###--------#--BBBV#######-------#--BEBBV########################");
                remarksw.push("horses/galley"); notesw.push("90000 cav and 360 galley @ 10.5 days");
                troopcounw.push([0,0,0,0,0,0,0,0,0,0,90000,0,0,0,360,0,0]);
                resw.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">5 sec sorc/galley</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##JBJBJ##-----##----##BJBJBJB##----##----#JBJBJBJBJ#----##----#JBJBJBJBJ#---H#######JBJBTBJBJ#######----#JBJBJBJBJ#-S-X##----#JBJBJBJBJ#----##----##BJBJBJB##JJ--##-----##JBJBJ##BBBBJ##------#######BBVVBB##---------#--JBV##VB##---------#--JBV###V###--------#---BBV#######-------#---JBBV########################");
                remarksw.push("sorc/galley"); notesw.push("156600 sorc and 314 galley @ 13.5 days");
                troopcounw.push([0,0,0,0,0,0,156600,0,0,0,0,0,0,0,314,0,0]);
                resw.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">vanqs+ports+senator</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##BBBBB##-----##----##BBGBGBB##----##----#BGBGBGBGB#----##----#BGBBBBBGB#----#######BBBGTGBBB#######----#BGBBBBBGB#PPJX##----#BGBGBGBGB#BBBB##----##BBGBGBB##BBBB##-----##BBBBB##BBBBB##------#######-BRRBB##---------#----R##RZ##---------#----R###R###--------#----SR#######-------#----MSR########################");
                remarksw.push("vanqs+senator+ports"); notesw.push("264k infantry @ 10 days");
                troopcounw.push([0,0,0,100000,0,164000,0,0,0,0,0,0,0,0,0,0,0]);
                resw.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">main hub</option>';
                layoutsw.push("[ShareString.1.3];########################-------#-----PP#####--------#-----PPP###---------#-----PPPP##---------#-----PPPP##------#######--PPPP##-----##-----##-PPPP##----##SLSMSAS##PPPP##----#LSLSMSASL#-PPP##----#MSLSMSASD#-PPP#######DSLSTSASM#######----#MSLSDSASD#----##----#ASLSDSASA#----##----##SLSDSAS##----##-----##-----##-----##------#######--RR--##---------#ZB--R##R-##---------#J---R###R###--------#-----R#######-------#------R########################");
                remarksw.push("main hub"); notesw.push("23 mil w/s 15 mil i/f 6200 carts 240 boats");
                troopcounw.push([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
                resw.push([0,0,0,0,1,500000,500000,500000,500000,0,0,0,0,1,0,0,0,0,0,500000,500000,500000,500000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">palace storage</option>';
                layoutsw.push("[ShareString.1.3]:########################-------#-----PP#####--------#-----PPP###---------#-----PPPP##---------#-----PPPP##------#######--PPPP##-----##SASLS##-PPPP##----##ASASLSL##PPPP##----#SASASLSLS#-PPP##----#SASASLSLS#JPPP#######SASA#LSLS#######----#SASASLSLS#----##----#SASASLSLS#----##----##ASASLSL##----##-----##SASLS##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksw.push("palace storage"); notesw.push("40 mil w/s 6200 carts");
                troopcounw.push([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
                resw.push([0,0,0,0,1,500000,500000,500000,500000,0,0,0,0,1,0,0,0,0,0,500000,500000,500000,500000]);
                ww++;
                selectbuttsw+='<option value="'+ww+'">palace feeder</option>';
                layoutsw.push("[ShareString.1.3];########################-PPPPPP#PPPPPPP#####--PPPPPP#PPPPPPPP###---PPPPPP#PPPPPPPPP##---PPPPPP#PPPPPPPPP##----PP#######PPPPPP##-----##----J##PPPPP##----##-A-----##PPPP##----#-SSS-----#PPPP##----#-AAA-----#PPPP#######-SSST----#######----#-LLL-----#----##----#-SSS-----#----##----##-L-----##----##-----##-----##-----##------#######--__--##---------#----_##_-##---------#----_###_###--------#-----_#######-------#------_########################");
                remarksw.push("palace feeder"); notesw.push("8.75 mil w/s 16400 carts");
                troopcounw.push([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
                resw.push([0,0,0,0,1,500000,500000,500000,500000,0,0,0,0,1,0,0,0,0,0,500000,500000,500000,500000]);
                selectbuttsw+='</select>';
                
                var selectbuttsl='<select id="andarlayoutl" style="font-size: 10px !important;margin-top:1%;margin-left:2%;width:45%;" class="regButton redb"><option value="0">Select land layout</option>';
                var ll=1;
                selectbuttsl+='<option value="'+ll+'">1 sec vanqs</option>';
                layoutsl.push("[ShareString.1.3]:########################-------#-------#####--------#--------###---------#---------##---------#---------##------#######------##-----##GBGBG##-----##----##BGBGBGB##----##----#GBGBGBGBG#----##----#GBGBGBGBG#----#######GBGBTBGBG#######----#GBGBGBGBG#----##----#GBGBGBGBG#----##----##BGBGBGB##----##GGGGG##GBGBG##-----##BBBBB-#######------##GGGGGG--H#---------##BBBBBB--J#---------###GGGG---X#--------#####BB----S#-------########################");
                remarksl.push("vanqs"); notesl.push("180000 vanqs @ 2 days");
                troopcounl.push([0,0,0,0,0,180000,0,0,0,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">2 sec vanqs</option>';
                layoutsl.push("[ShareString.1.3]:########################BBB--JX#-------#####BGBG--PP#--------###-BBBBB-MS#---------##-BGBGB--H#---------##-BGBGB#######------##-ZBB-##BBBBB##-----##----##BBGBGBB##----##----#BGBGBGBGB#----##----#BGBBBBBGB#----#######BGBGTGBGB#######----#BGBBBBBGB#----##----#BGBGBGBGB#----##----##BBGBGBB##----##-----##BBBBB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("vanqs"); notesl.push("264000 vanqs @ 6 days");
                troopcounl.push([0,0,0,0,0,264000,0,0,0,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">3 sec vanqs raiding</option>';
                layoutsl.push("[ShareString.1.3];########################------S#-------#####BBBB--PP#--------###BGBGB--JP#---------##BBBBB---X#---------##BGBGB-#######------##BBBBB##BBBBB##-----##--GB##BBGBGBB##----##----#BBBBBBBBB#----##----#BGBGBGBGB#----#######BBBBTBBBB#######----#BGBGBGBGB#----##----#BBBBBBBBB#----##----##BBGBGBB##----##-----##BBBBB##-----##------#######--__--##---------#----_##_-##---------#----_###_###--------#-----_#######-------#------_########################");
                remarksl.push("vanqs"); notesl.push("308000 vanqs @ 10.5 days");
                troopcounl.push([0,0,0,0,0,308000,0,0,0,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">2 sec rangers</option>';
                layoutsl.push("[ShareString.1.3]:########################BB---JX#-------#####BGBGB-PP#--------###-BGBGB-MS#---------##-BGBGB--H#---------##-BGBGB#######------##--BBB##BGBGB##-----##----##BBGBGBB##----##----#BGBGBGBGB#----##----#BGBGBGBGB#----#######BGBGTGBGB#######----#BGBGBGBGB#----##----#BGBGBGBGB#----##----##BBGBGBB##----##-----##BBBBB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("rangers/triari"); notesl.push("236000 inf @ 6.5 days");
                troopcounl.push([0,0,156000,80000,0,0,0,0,0,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">6 sec praetors</option>';
                layoutsl.push("[ShareString.1.3]:########################BB---JX#-------#####BZBZB-PP#--------###-BZBZB-MS#---------##-BZBZB--H#---------##-BZBZB#######------##--BBB##BZBZB##-----##----##ZBZBZBZ##----##----#BZBZBZBZB#----##----#BZBZBZBZB#----#######BZBZTZBZB#######----#BZBZBZBZB#----##----#BZBZBZBZB#----##----##BBZBZBB##----##-----##BZBZB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("praetors"); notesl.push("110000 praetors @ 7.5 days");
                troopcounl.push([0,0,0,0,0,0,0,0,0,110000,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">4 sec horses</option>';
                layoutsl.push("[ShareString.1.3]:########################BB---JX#-------#####BEBEB-PP#--------###-BEBEB-MS#---------##-BEBEB--H#---------##-BEBEB#######------##--ZBB##BEBEB##-----##----##EBEBEBE##----##----#BEBEBEBEB#----##----#BEBEBEBEB#----#######BEBETEBEB#######----#BEBEBEBEB#----##----#BEBEBEBEB#----##----##BBEBEBE##----##-----##BEBEB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("horses"); notesl.push("106000 horses @ 5 days");
                troopcounl.push([0,0,0,0,0,0,0,0,0,0,106000,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">5 sec arbs</option>';
                layoutsl.push("[ShareString.1.3]:########################BB---JX#-------#####BEBEB-PP#--------###-BEBEB-MS#---------##-BEBEB--H#---------##-BEBEB#######------##--BBB##BEBEB##-----##----##EBEBEBE##----##----#BEBEBEBEB#----##----#BEBEBEBEB#----#######BEBETEBEB#######----#BEBEBEBEB#----##----#BEBEBEBEB#----##----##BBEBEBB##----##-----##BEBEB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("arbs"); notesl.push("110000 arbs @ 6.5 days");
                troopcounl.push([0,0,0,0,0,0,0,0,110000,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">6 sec arbs</option>';
                layoutsl.push("[ShareString.1.3]:########################BB---JX#-------#####BEBEB-PP#--------###-BBBEB-MS#---------##-BEBEB--H#---------##-BEBEB#######------##--BBB##BBBBB##-----##----##BBEBEBB##----##----#BEBEBEBEB#----##----#BEBEBEBEB#----#######BEBETEBEB#######----#BEBEBEBEB#----##----#BEBEBEBEB#----##----##BBEBEBB##----##-----##BBBBB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("arbs"); notesl.push("124000 arbs @ 8.5 days");
                troopcounl.push([0,0,0,0,0,0,0,0,124000,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">4 sec sorc</option>';
                layoutsl.push("[ShareString.1.3]:########################BJBJ--X#-------#####JBJBJ--S#--------###-JBJBJ--M#---------##-JBJBJ--H#---------##-JBJBJ#######------##-ZBJB##JBJBJ##-----##----##BJBJBJB##----##----#JBJBJBJBJ#----##----#JBJBJBJBJ#----#######JBJBTBJBJ#######----#JBJBJBJBJ#----##----#JBJBJBJBJ#----##----##BJBJBJB##----##-----##JBJBJ##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("sorc"); notesl.push("176000 sorc @ 8 days");
                troopcounl.push([0,0,0,0,0,0,176000,0,0,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">5 sec sorc</option>';
                layoutsl.push("[ShareString.1.3]:########################BBB---X#-------#####BJBJB--P#--------###-BJBJB-MS#---------##-BJBJB--H#---------##-BJBJB#######------##-ZBBB##BJBJB##-----##----##JBJBJBJ##----##----#BJBJBJBJB#----##----#BJBJBJBJB#----#######BJBJTJBJB#######----#BJBJBJBJB#----##----#BJBJBJBJB#----##----##BBJBJBB##----##-----##BJBJB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("sorc"); notesl.push("224000 sorc @ 13 days");
                troopcounl.push([0,0,0,0,0,0,224000,0,0,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">10 sec druids</option>';
                layoutsl.push("[ShareString.1.3]:########################-J----X#-------#####JBJB--MS#--------###BJBJB---H#---------##BJBJB----#---------##BJBJB-#######------##BJBJB##BJBJB##-----##-JBJ##JBJBJBJ##----##----#BJBJBJBJB#----##----#BJBJBJBJB#----#######BJBJTJBJB#######----#BJBJBJBJB#----##----#BJBJBJBJB#----##----##JBJBJBJ##----##-----##BJBJB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("druids"); notesl.push("102000 druids @ 12 days");
                troopcounl.push([0,0,0,0,0,0,0,0,0,0,0,102000,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">scorp/rams</option>';
                layoutsl.push("[ShareString.1.3]:########################BBYB--X#-------#####BYBYB---#--------###-BYBYB-MS#---------##-BYBYB--H#---------##-BYBYB#######------##-BYBB##BYBYB##-----##----##YBYBYBY##----##----#BYBYBYBYB#----##----#BYBYBYBYB#----#######BYBYTYBYB#######----#BYBYBYBYB#----##----#BYBYBYBYB#----##----##YBYBYBY##----##-----##BYBYB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("scorp/rams"); notesl.push("21600 siege engines @ 7.5 days");
                troopcounl.push([0,0,0,0,0,0,0,0,0,0,0,0,5500,16100,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                ll++;
                selectbuttsl+='<option value="'+ll+'">ballista</option>';
                layoutsl.push("[ShareString.1.3]:########################BBBB--X#-------#####BYBYB---#--------###-BYBYB-MS#---------##-BYBYB--H#---------##-BYBYB#######------##-BBBB##BBBBB##-----##----##BBYBYBB##----##----#BYBYBYBYB#----##----#BYBYBYBYB#----#######BYBYTYBYB#######----#BYBYBYBYB#----##----#BYBYBYBYB#----##----##BBYBYBB##----##-----##BBBBB##-----##------#######------##---------#---------##---------#---------###--------#--------#####-------#-------########################");
                remarksl.push("ballista"); notesl.push("25600 siege engines @ 10.5 days");
                troopcounl.push([0,25600,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
                resl.push([0,0,0,0,1,150000,220000,150000,350000,0,0,0,0,1,0,0,0,0,0,150000,220000,150000,350000]);
                selectbuttsl+='</select>';


                $('#removeoverlayGo').after(selectbuttsl);
                $('#andarlayoutl').after(selectbuttsw);
                $('#andarlayoutl').change(function() {
                    var newlayout=currentlayout;
                    //console.log($('#andarlayoutl').val());
                    for (var j=1; j<layoutsl.length; j++) {
                        if ($('#andarlayoutl').val()==j) {
                            for (var i=20; i<currentlayout.length;i++) {
                                var tmpchar=layoutsl[j].charAt(i);
                                var cmp=new RegExp(tmpchar);
                                if (!(cmp.test(emptyspots))) {
                                    //console.log(String(cmp));
                                    newlayout=newlayout.replaceAt(i,tmpchar);
                                    //currentlayout=currentlayout.replaceAt(i,tmpchar);
                                }
                            }
                            //console.log(newlayout);
                            //console.log(currentlayout);
                            $('#removeoverlayGo').click();
                            setTimeout(function(){$('#overlaytextarea').val(newlayout);},300);
                            setTimeout(function(){$('#applyoverlayGo').click();},300);
                            if ($("#addnotes").prop("checked")==true) {
                                setTimeout(function(){$('#citynotesTab').click();},200);
                                $('#CNremarks').val("");
                                $('#citynotestextarea').val("");
                                setTimeout(function(){$('#citnotesaveb').click();},100);
                                $('#CNremarks').val(remarksl[j]);
                                $('#citynotestextarea').val(notesl[j]);
                                setTimeout(function(){$('#citnotesaveb').click(); },100);
                            }
                            var aa=city.mo;
                            //console.log(aa);
                            if ($("#addtroops").prop("checked")==true) {
                                for (var k in troopcounl[j]) {
                                    aa[9+Number(k)]=troopcounl[j][k];
                                }
                            }
                            if ($("#addwalls").prop("checked")==true) {
                                aa[26]=1;
                            }
                            if ($("#addtowers").prop("checked")==true) {
                                aa[27]=1;
                            }
                            if ($("#addhub").prop("checked")==true) {
                                var hubs={cid:[],distance:[]};
                                $.each(pdata.clc, function(key, value) {
                                    if (key==$("#selHub").val()) {
                                        hubs.cid=value;
                                    }
                                });
                                for (var i in hubs.cid) {
                                    var tempx=Number(hubs.cid[i] % 65536);
                                    var tempy=Number((hubs.cid[i]-tempx)/65536);
                                    hubs.distance.push(Math.sqrt((tempx-city.x)*(tempx-city.x)+(tempy-city.y)*(tempy-city.y)));
                                }
                                var mindist = Math.min.apply(Math, hubs.distance);
                                var nearesthub=hubs.cid[hubs.distance.indexOf(mindist)];
                                resl[j][14]=nearesthub;
                                resl[j][15]=nearesthub;
                            } else {
                                resl[j][14]=0;
                                resl[j][15]=0;
                            }
                            if ($("#addres").prop("checked")==true) {
                                resl[j][5]=$("#woodin").val();
                                resl[j][6]=$("#stonein").val();
                                resl[j][7]=$("#ironin").val();
                                resl[j][8]=$("#foodin").val();
                                resl[j][19]=$("#woodin").val();
                                resl[j][20]=$("#stonein").val();
                                resl[j][21]=$("#ironin").val();
                                resl[j][22]=$("#foodin").val();
                                for (var k in resl[j]) {
                                    aa[28+Number(k)]=resl[j][k];
                                }
                            }
                            if ($("#addbuildings").prop("checked")==true) {
                                aa[51]=[1,$("#cablev").val()];
                                aa[1]=1;
                            }
                            
                            //console.log(aa);
                           //var aaa=JSON.stringify(aa);
                            var dat={a:JSON.stringify(aa),b:city.cid};
                            //console.log(dat);
                            jQuery.ajax({url: 'includes/mnio.php',type: 'POST',aysnc:false,data: dat});
                            
                        }
                    }
                });
                $('#andarlayoutw').change(function() {
                    var newlayout=currentlayout;
                    //console.log($('#andarlayoutw').val());
                    for (var j=1; j<layoutsw.length; j++) {
                        if ($('#andarlayoutw').val()==j) {
                            for (var i=20; i<currentlayout.length;i++) {
                                var tmpchar=layoutsw[j].charAt(i);
                                var cmp=new RegExp(tmpchar);
                                if (!(cmp.test(emptyspots))) {
                                    //console.log(String(cmp));
                                    newlayout=newlayout.replaceAt(i,tmpchar);
                                }
                            }
                            $('#removeoverlayGo').click();
                            setTimeout(function(){$('#overlaytextarea').val(newlayout);},300);
                            setTimeout(function(){$('#applyoverlayGo').click();},300);
                            if ($("#addnotes").prop("checked")==true) {
                                setTimeout(function(){$('#citynotesTab').click();},200);
                                $('#CNremarks').val("");
                                $('#citynotestextarea').val("");
                                setTimeout(function(){$('#citnotesaveb').click();},100);
                                $('#CNremarks').val(remarksw[j]);
                                $('#citynotestextarea').val(notesw[j]);
                                setTimeout(function(){$('#citnotesaveb').click(); },100);
                            }
                            var aa=city.mo;
                            //console.log(aa);
                            if ($("#addtroops").prop("checked")==true) {
                                for (var k in troopcounw[j]) {
                                    aa[9+Number(k)]=troopcounw[j][k];
                                }
                            }
                            if ($("#addwalls").prop("checked")==true) {
                                aa[26]=1;
                            }
                            if ($("#addtowers").prop("checked")==true) {
                                aa[27]=1;
                            }
                            if ($("#addhub").prop("checked")==true) {
                                var hubs={cid:[],distance:[]};
                                $.each(pdata.clc, function(key, value) {
                                    if (key==$("#selHub").val()) {
                                        hubs.cid=value;
                                    }
                                });
                                for (var i in hubs.cid) {
                                    var tempx=Number(hubs.cid[i] % 65536);
                                    var tempy=Number((hubs.cid[i]-tempx)/65536);
                                    hubs.distance.push(Math.sqrt((tempx-city.x)*(tempx-city.x)+(tempy-city.y)*(tempy-city.y)));
                                }
                                var mindist = Math.min.apply(Math, hubs.distance);
                                var nearesthub=hubs.cid[hubs.distance.indexOf(mindist)];
                                resw[j][14]=nearesthub;
                                resw[j][15]=nearesthub;
                            } else {
                                resw[j][14]=0;
                                resw[j][15]=0;
                            }
                            if ($("#addres").prop("checked")==true) {
                                resw[j][5]=$("#woodin").val();
                                resw[j][6]=$("#stonein").val();
                                resw[j][7]=$("#ironin").val();
                                resw[j][8]=$("#foodin").val();
                                resw[j][19]=$("#woodin").val();
                                resw[j][20]=$("#stonein").val();
                                resw[j][21]=$("#ironin").val();
                                resw[j][22]=$("#foodin").val();
                                for (var k in resw[j]) {
                                    aa[28+Number(k)]=resw[j][k];
                                }
                            }
                            if ($("#addbuildings").prop("checked")==true) {
                                aa[51]=[1,$("#cablev").val()];
                                aa[1]=1;
                            }
                            //console.log(aa);
                           //var aaa=JSON.stringify(aa);
                            var dat={a:JSON.stringify(aa),b:city.cid};
                            //console.log(dat);
                            jQuery.ajax({url: 'includes/mnio.php',type: 'POST',aysnc:false,data: dat});
                        }
                    }
                });
            },500);
        });
    });
    
    $(document).ready(function() {
        $("#loccavwarconGo").click(function() {
            //createTable();
            setTimeout(function(){getDugRows();}, 1000);
        });
        
        $("#raidmantab").click(function() {
            setTimeout(function(){getDugRows();}, 1000);
        });
        //$("#raidDungGo").click(function() {
        //    setTimeout(function(){setbossloot();}, 1000);
        //});
    });
    
    // getting research and faith info
    $(document).ready(function() {
        /*jQuery.ajax({url: 'includes/gaLoy.php',type: 'POST',aysnc:false,
                         success: function(data) {
                             var ldata=JSON.parse(data);
                             setloyal(ldata);
                         }
                        });
        var reslvl;
        function setloyal(ldata) {
            //console.log(ldata);
            var faith=0;
            //domdis
            $.each(ldata.t, function(key, value) {
                if (key==6) {
                    $.each(this, function(key, value) {
                        ylannafaith+=this.f;
                    });
                }
                if (key==3) {
                    $.each(this, function(key, value) {
                        domdisfaith+=this.f;
                    });
                }
                if (key==8) {
                    $.each(this, function(key, value) {
                        naerafaith+=this.f;
                    });
                }               
            });
            ttres[5]+=0.5*vexfaith/100;
            ttres[7]+=0.5*vexfaith/100;
            ttres[6]+=0.5*vexfaith/100;
            ttres[10]+=0.5*vexfaith/100;
            ttres[11]+=0.5*vexfaith/100;
            for (var i in ttspeedres) {
                    if (isinf[i] || iscav[i]) {
                        ttspeedres[i]+=0.5*ibriafaith/100;
                    }
            }
            ttres[0]+=0.5*ylannafaith/100;
            ttres[1]+=0.5*ylannafaith/100;
            ttres[14]+=0.5*ylannafaith/100;
            ttres[15]+=0.5*ylannafaith/100;
            ttres[2]+=0.5*naerafaith/100;
            ttres[3]+=0.5*ylannafaith/100;
            ttres[4]+=0.5*ylannafaith/100;
            ttres[8]+=0.5*ylannafaith/100;
            ttres[9]+=0.5*ylannafaith/100;
            ttres[12]+=0.5*cyndrosfaith/100;
            ttres[13]+=0.5*cyndrosfaith/100;
            ttres[16]+=0.5*cyndrosfaith/100;
            ttspeedres[1]+=0.5*domdisfaith/100;
            ttspeedres[12]+=0.5*domdisfaith/100;
            ttspeedres[13]+=0.5*domdisfaith/100;
            ttspeedres[14]+=0.5*domdisfaith/100;
            ttspeedres[15]+=0.5*domdisfaith/100;
            ttspeedres[16]+=0.5*domdisfaith/100;
            }*/
        setTimeout(function(){
        $(".resPop").each(function() {
            if($(this).attr('data')==8) { //inf speed
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else var reslvl=Number(ranktext.match(/\d+/gi));
                for (var i in ttspeedres) {
                    if (isinf[i]) {
                ttspeedres[i]+=resbonus[reslvl];
                    }
                }
                ttspeedres[6]+=resbonus[reslvl];
                //console.log(ttres[2]);
            }
            if($(this).attr('data')==9) { //cav speed
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else var reslvl=Number(ranktext.match(/\d+/gi));
                for (var i in ttspeedres) {
                    if (iscav[i]) {                        
                ttspeedres[i]+=resbonus[reslvl];
                    }
                }
                ttspeedres[11]+=resbonus[reslvl];
                //console.log(ttres[2]);
            }
            if($(this).attr('data')==13) { //navy speed
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else var reslvl=Number(ranktext.match(/\d+/gi));
                ttspeedres[14]+=resbonus[reslvl];
                ttspeedres[15]+=resbonus[reslvl];
                ttspeedres[16]+=resbonus[reslvl];
                //console.log(ttres[2]);
            }
            if($(this).attr('data')==30) { //rangers
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[2]+=resbonus[reslvl];
                //console.log(ttres[2]);
            }
            if($(this).attr('data')==31) { //triari
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[3]+=resbonus[reslvl];
            }
            if($(this).attr('data')==32) { //priestess
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[4]+=resbonus[reslvl];
            }
            if($(this).attr('data')==33) { //vanqs
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[5]+=resbonus[reslvl];
            }
            if($(this).attr('data')==34) { //sorcs
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[6]+=resbonus[reslvl];
            }
            if($(this).attr('data')==35) { //arbs
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[8]+=resbonus[reslvl];
            }
            if($(this).attr('data')==36) { //praetors
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[9]+=resbonus[reslvl];
            }
            if($(this).attr('data')==37) { //horseman
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[10]+=resbonus[reslvl];
            }
            if($(this).attr('data')==38) { //druid
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[11]+=resbonus[reslvl];
            }
            if($(this).attr('data')==43) { //stinger
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[15]+=resbonus[reslvl];
            }
            if($(this).attr('data')==44) { //galley
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[14]+=resbonus[reslvl];
            }
            if($(this).attr('data')==45) { //warships
                var ranktext=$(this).text();
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[16]+=resbonus[reslvl];
            }
            if($(this).attr('data')==46) { //scouts
                var ranktext=$(this).text();
                //console.log(ranktext);
                var cmp=new RegExp("complete")
                if (cmp.test(ranktext)) {
                    reslvl=12;
                }
                else
                    var reslvl=Number(ranktext.match(/\d+/gi));
                ttres[7]+=resbonus[reslvl];
            }
        });
        },5000);
    });
    
    document.getElementById('raidDungGo').onclick = function() {
            //createTable();
        setTimeout(function(){setbossloot();}, 1000);
        }; 


    function createTable() {
        $('#Andardiv').remove();
        var ptworow=$("#Progress").html();
        if (ptworow==0)
        {ptworow=100;}
	var plevorow=$("#dunglevelregion").html();
	var ptropneed = Math.ceil(loot[plevorow]*((100-ptworow)*0.008+1)/10);
        var outtable="<div id='Andardiv' style='width:500px;height:330px;background-color: #E2CBAC;-moz-border-radius: 10px;-webkit-border-radius: 10px;border-radius: 10px;border: 4px ridge #DAA520;position:absolute;right:10px;top:100px; z-index:1000000;'><div class=\"popUpBar\"> <span class=\"ppspan\">Suggested Raiding Numbers - Caver progress "+ptworow+"%</span> <button id=\"AndarZ\" onclick=\"$('#Andardiv').remove();\" class=\"xbutton yellowb\"><div id=\"xbuttondiv\"><div><div id=\"centxbuttondiv\"></div></div></div></button></div><div class=\"popUpWindow\">";
        outtable+="<table><thead><th>Lvl</th><th>Estimated Loot</th><th>Vanqs/Rangers<br>druids</th><th>Sorcs</th><th>Praetors</th><th>Arbs/Horses</th></thead>";
        outtable+="<tbody><tr><td>1</td><td>400</td><td>"+Math.ceil(loot[1]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[1]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[1]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[1]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>2</td><td>1000</td><td>"+Math.ceil(loot[2]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[2]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[2]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[2]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>3</td><td>4500</td><td>"+Math.ceil(loot[3]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[3]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[3]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[3]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>4</td><td>15000</td><td>"+Math.ceil(loot[4]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[4]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[4]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[4]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>5</td><td>33000</td><td>"+Math.ceil(loot[5]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[5]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[5]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[5]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>6</td><td>60000</td><td>"+Math.ceil(loot[6]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[6]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[6]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[6]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>7</td><td>120000</td><td>"+Math.ceil(loot[7]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[7]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[7]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[7]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>8</td><td>201000</td><td>"+Math.ceil(loot[8]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[8]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[8]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[8]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>9</td><td>300000</td><td>"+Math.ceil(loot[9]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[9]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[9]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[9]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="<tr><td>10</td><td>446000</td><td>"+Math.ceil(loot[10]*((100-ptworow)*0.008+1)/10)+"</td><td>"+Math.ceil(loot[10]*((100-ptworow)*0.008+1)/5)+"</td><td>"+Math.ceil(loot[10]*((100-ptworow)*0.008+1)/20)+"</td><td>"+Math.ceil(loot[10]*((100-ptworow)*0.008+1)/15)+"</td></tr>";
        outtable+="</tbody></table>";
        outtable+="</div></div>";
        $( "body" ).append(outtable);
        $( "#Andardiv" ).draggable({ handle: ".popUpBar" , containment: "window", scroll: false});


    }
    function setbossloot() {
        var ttm=[0];
        var ttc=0;
        var bosslvl=$("#dunglevelregion").html();
        var bosstype=$("#dungtypespot").html();
        var tnumb=[0];
        $("#raidingTable tr").each(function() {
            var temp=$(this).find("td:nth-child(3)").text();
            var n = temp.search("/");
            temp=temp.substring(0,n);
            temp=temp.replace(",","");
            var troops=Number(temp);
            //console.log(troops);
            var temp1=$(this).attr('id');
            var tt=Number(temp1.match(/\d+/gi));
            //console.log(temp1);
            if (tt!==7) {
                if (troops>0) {
                    ttc+=1;
                    ttm[ttc-1]=tt;
                    tnumb[ttc-1]=troops;
                }
            }
        });
        
            if (bosstype=="Triton") {
                for (i=0; i<ttc+1; i++) {
                    $('#Andardiv').remove();
                    if (ttm[i]>13) {
                        if (isart[ttm[i]]) {
                            var amount=Math.ceil(bossdefw[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                            if (amount<=tnumb[i]) {
                                $('#raidIP'+ttm[i]).val(amount);
                            } else {
                                message="Impossible action, one needs more troops at least " + amount + " " + ttname[ttm[i]]+"!";
                                errorgo(message);
                            }
                        } else {
                            var amount=Math.ceil(bossdef[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                            if (amount<=tnumb[i]) {
                                $('#raidIP'+ttm[i]).val(amount);
                            } else {
                                message="Impossible action, one needs more troops at least + " " + ttname[ttm[i]]+"!";
                                errorgo(message);
                            }
                        }
                    }
                }
            }
                    else if (bosstype=="Cyclops") {
                        for (i=0; i<ttc+1; i++) {
                            $('#Andardiv').remove();
                            if (ttm[i]<13) {
                                if (iscav[ttm[i]]) {
                                    var amount=Math.ceil(bossdefw[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Impossible action, one needs more troops at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                } else {
                                    var amount=Math.ceil(bossdef[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Impossible action, one needs more troops at least" + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                }
                            }
                        }
                    }
                    else if (bosstype=="Andar's Colosseum Challenge") {
                        for (i=0; i<ttc+1; i++) {
                            $('#Andardiv').remove();
                            if (ttm[i]<13) {
                                if (iscav[ttm[i]]) {
                                    var amount=Math.ceil(bossdefw[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Impossible action, one needs more troops at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                } else {
                                    var amount=Math.ceil(bossdef[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Impossible action, one needs more troops at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                }
                            }
                        }
                    }
                    else if (bosstype=="Dragon") {
                        for (i=0; i<ttc+1; i++) {
                            $('#Andardiv').remove();
                            if (ttm[i]<13) {
                                if (isinf[ttm[i]]) {
                                    var amount=Math.ceil(bossdefw[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Impossible action, one needs more troops at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                } else {
                                    var amount=Math.ceil(bossdef[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Impossible action, one needs more troops at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                }
                            }
                        }
                    }
                    else if (bosstype=="Romulus and Remus") {
                        for (i=0; i<ttc+1; i++) {
                            $('#Andardiv').remove();
                            if (ttm[i]<13) {
                                if (isinf[ttm[i]]) {
                                    var amount=Math.ceil(bossdefw[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Impossible action, one needs more troops at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                } else {
                                    var amount=Math.ceil(bossdef[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Impossible action, one needs more troops at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                }
                            }
                        }
                    }
                    else if (bosstype=="Gorgon") {
                        for (i=0; i<ttc+1; i++) {
                            $('#Andardiv').remove();
                            if (ttm[i]<13) {
                                if (ismgc[ttm[i]]) {
                                    var amount=Math.ceil(bossdefw[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Impossible action, one needs more troops at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                } else {
                                    var amount=Math.ceil(bossdef[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Impossible action, one needs more troops at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                }
                            }
                        }
                    }
                    else if (bosstype=="GM Gordy") {
                        for (i=0; i<ttc+1; i++) {
                            $('#Andardiv').remove();
                            if (ttm[i]<13) {
                                if (ismgc[ttm[i]]) {
                                    var amount=Math.ceil(bossdefw[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    if (amount<tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Impossible action, one needs more troops at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                } else {
                                    var amount=Math.ceil(bossdef[bosslvl-1]*4/(ttres[ttm[i]]*ttattack[ttm[i]]));
                                    if (amount<=tnumb[i]) {
                                        $('#raidIP'+ttm[i]).val(amount);
                                    } else {
                                        message="Impossible action, one needs more troops at least " + amount + " " + ttname[ttm[i]]+"!";
                                        errorgo(message);
                                    }
                                }
                            }
                        }
                    }
                    else { createTable();}
    }                                 




    function getDugRows() {
        //$('#maxcavcomp').remove();
        var maxcomp='<select id="maxcavcomp" style="font-size: 10px !important;width:27%;height:27px;" class="redsel"><option value="75">Maximum completion</option>';
        maxcomp+='<option value="100">100%</option><option value="85">85%</option><option value="70">70%</option><option value="55">55%</option><option value="40">40%</option><option value="25">25%</option><option value="75">75%(default)</option>';
        $('#secwarCspan').width('35%');
        $('#raidLTinfogselWC').width('20%');
        if (!$('#maxcavcomp').length) {
            $('#raidLTinfogselWC').before(maxcomp);
        }
        $("#dungloctab tr").each(function() {
            var buttont=$(this).find( "button");
            var buttonid=buttont.attr('id');
            var tworow=$(this).find( "td:nth-child(2)").text();
            var threerow=$(this).find( "td:nth-child(3)").text();
            if(buttonid) {
                var reference=buttonid.substring(8);
                buttont.addClass('Andar1');
                buttont.attr('dd',reference);
                buttont.attr('dt',tworow);
                buttont.attr('dtt',threerow);
            }
        
        
        $(buttont).click(function() {
            var count=Number($('.splitRaid').children('option').length)-1;
            var troopshome=[0];
            var ttc=0;
            var tt=["non"];
            var ttm=[0];
            var butid=$(this).attr('dd');
            var dungtext=$(this).attr('dt');
            var progress=$(this).attr('dtt');
            var temp=dungtext.match(/\d+/gi);
            numbs[0]=Number(temp);
            temp=progress.match(/\d+/gi);
            numbs[1]=Math.min(temp,$('#maxcavcomp').val());
            //console.log(numbs[1]);
            setTimeout(function(){


                $(".tninput").each(function() {
                    ttc+=1;
                    var trpinpid=$(this).attr('id');
                    //tt[ttc-1]=trpinpid;
                    ttm[ttc-1]=Number(trpinpid.match(/\d+/gi));
                });
                //console.log(ttc);
                //console.log(ttm);
                if(ttc==1) {
                    troopshome[0]=$('#rval'+ttm[0]).val();
                    //console.log(troopshome);
                    numbs[2]=Math.ceil(loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[0]]);
                    if(Number(troopshome)>=numbs[2]) {
                        $('#rval'+ttm[0]).val(numbs[2]);
                        $('#wcraidunti').val(1);
                        $('#inprettimewc').hide();
                    } else {
                        message="Impossible action, one needs more troops at least " + numbs[2] + " " + ttname[ttm[0]]+"!";
                        errorgo(message);
                        $('#wcraidunti').val(3);
                        $('#inprettimewc').show();
                                }
                    if((Number(troopshome)/Number(numbs[2]))<count) {
                        count=Number(troopshome)/Number(numbs[2]);
                    }
                }
                if(ttc==2) {
                    if (ttm[1]==14) {
                        troopshome[0]=$('#rval'+ttm[0]).val();
                        numbs[2]=Math.ceil(loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[0]]);
                        if(Number(troopshome)>=numbs[2]) {
                            $('#rval'+ttm[0]).val(numbs[2]);
                            $('#wcraidunti').val(1);
                            $('#inprettimewc').hide();
                        } else {
                            message="Impossible action, one needs more troops at least " + numbs[2] + " " + ttname[ttm[0]]+"!";
                            errorgo(message);
                            $('#wcraidunti').val(3);
                            $('#inprettimewc').show();
                                }
                        if((Number(troopshome)/Number(numbs[2]))<count) {
                            count=Number(troopshome)/Number(numbs[2]);
                        }
                        $('#rval'+ttm[1]).val(0);
                    } else {                      
                        troopshome[0]=$('#rval'+ttm[0]).val();
                        troopshome[1]=$('#rval'+ttm[1]).val();
                        var ratio=[troopshome[0]*ttloot[ttm[0]]/(troopshome[1]*ttloot[ttm[1]]+troopshome[0]*ttloot[ttm[0]]),troopshome[1]*ttloot[ttm[1]]/(troopshome[1]*ttloot[ttm[1]]+troopshome[0]*ttloot[ttm[0]])];
                        //console.log(ratio);
                        numbs[2]=Math.ceil(ratio[0]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[0]]);
                        //console.log(numbs[2]);
                        if (troopshome[0]>0) {
                            if(Number(troopshome[0])>=numbs[2]) {
                                $('#rval'+ttm[0]).val(numbs[2]);
                                $('#wcraidunti').val(1);
                                $('#inprettimewc').hide();
                            }  else {
                                message="Impossible action, one needs more troops at least " + numbs[2] + " " + ttname[ttm[0]]+"!";
                                errorgo(message);
                                $('#wcraidunti').val(3);
                                $('#inprettimewc').show();
                            }
                            if((Number(troopshome[0])/Number(numbs[2]))<count) {
                                count=Number(troopshome[0])/Number(numbs[2]);
                            }  
                            numbs[2]=Math.ceil(ratio[1]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[1]]);
                            if(Number(troopshome[1])>=numbs[2]) {
                                $('#rval'+ttm[1]).val(numbs[2]);
                            }
                        }
                        else {
                            numbs[2]=Math.ceil(ratio[1]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[1]]);
                            if(Number(troopshome[1])>=numbs[2]) {
                                $('#rval'+ttm[1]).val(numbs[2]);
                                $('#wcraidunti').val(1);
                                $('#inprettimewc').hide();
                            } else {
                                message="Impossible action, one needs more troops at least " + numbs[2] + " " + ttname[ttm[1]]+"!";
                                errorgo(message);
                                $('#wcraidunti').val(3);
                                $('#inprettimewc').show();
                            }
                            if((Number(troopshome[1])/Number(numbs[2]))<count) {
                                count=Number(troopshome[1])/Number(numbs[2]);
                            }
                        }
                    }
                }
                if (ttc==3) {
                    if (ttm[2]==14) {
                        troopshome[0]=$('#rval'+ttm[0]).val();
                        troopshome[1]=$('#rval'+ttm[1]).val();
                        var ratio=[troopshome[0]*ttloot[ttm[0]]/(troopshome[1]*ttloot[ttm[1]]+troopshome[0]*ttloot[ttm[0]]),troopshome[1]*ttloot[ttm[1]]/(troopshome[1]*ttloot[ttm[1]]+troopshome[0]*ttloot[ttm[0]])];
                        //console.log(ratio);
                        numbs[2]=Math.ceil(ratio[0]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[0]]);
                        //console.log(numbs[2]);
                        if (troopshome[0]>0) {
                            if(Number(troopshome[0])>=numbs[2]) {
                                $('#rval'+ttm[0]).val(numbs[2]);
                                $('#wcraidunti').val(1);
                                $('#inprettimewc').hide();
                            }  else {
                                message="Impossible action, one needs more troops at least " + numbs[2] + " " + ttname[ttm[0]]+"!";
                                errorgo(message);
                                $('#wcraidunti').val(3);
                                $('#inprettimewc').show();
                            }
                            if((Number(troopshome[0])/Number(numbs[2]))<count) {
                                count=Number(troopshome[0])/Number(numbs[2]);
                            }  
                            numbs[2]=Math.ceil(ratio[1]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[1]]);
                            if(Number(troopshome[1])>=numbs[2]) {
                                $('#rval'+ttm[1]).val(numbs[2]);
                            }
                        }
                        else {
                            numbs[2]=Math.ceil(ratio[1]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[1]]);
                            if(Number(troopshome[1])>=numbs[2]) {
                                $('#rval'+ttm[1]).val(numbs[2]);
                                $('#wcraidunti').val(1);
                                $('#inprettimewc').hide();
                            } else {
                                message="Impossible action, one needs more troops at least " + numbs[2] + " " + ttname[ttm[1]]+"!";
                                errorgo(message);
                                $('#wcraidunti').val(3);
                                $('#inprettimewc').show();
                            }
                            if((Number(troopshome[1])/Number(numbs[2]))<count) {
                                count=Number(troopshome[1])/Number(numbs[2]);
                            }
                        }
                        $('#rval'+ttm[2]).val(0);
                    }
                    else {
                        troopshome[0]=$('#rval'+ttm[0]).val();
                        troopshome[1]=$('#rval'+ttm[1]).val();
                        troopshome[2]=$('#rval'+ttm[2]).val();
                        var ratio=[troopshome[0]*ttloot[ttm[0]]/(troopshome[2]*ttloot[ttm[2]]+troopshome[1]*ttloot[ttm[1]]+troopshome[0]*ttloot[ttm[0]])];
                        ratio[1]=troopshome[1]*ttloot[ttm[1]]/(troopshome[2]*ttloot[ttm[2]]+troopshome[1]*ttloot[ttm[1]]+troopshome[0]*ttloot[ttm[0]]);
                        ratio[2]=troopshome[2]*ttloot[ttm[2]]/(troopshome[2]*ttloot[ttm[2]]+troopshome[1]*ttloot[ttm[1]]+troopshome[0]*ttloot[ttm[0]]);
                        //console.log(ratio);
                        if (troopshome[0]>0) {
                            numbs[2]=Math.ceil(ratio[0]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[0]]);
                            //console.log(numbs[2]);
                            if(Number(troopshome[0])>=numbs[2]) {
                                $('#rval'+ttm[0]).val(numbs[2]);
                                $('#wcraidunti').val(1);
                                $('#inprettimewc').hide();
                            } else {
                                message="Impossible action, one needs more troops at least " + numbs[2] + " " + ttname[ttm[0]]+"!";
                                errorgo(message);
                                $('#wcraidunti').val(3);
                                $('#inprettimewc').show();
                            }
                            if((Number(troopshome[0])/Number(numbs[2]))<count) {
                                count=Number(troopshome[0])/Number(numbs[2]);
                            }
                            numbs[2]=Math.ceil(ratio[1]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[1]]);
                            if(Number(troopshome[1])>=numbs[2]) {
                                $('#rval'+ttm[1]).val(numbs[2]);
                            }
                            numbs[2]=Math.ceil(ratio[2]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[2]]);
                            if(Number(troopshome[2])>=numbs[2]) {
                                $('#rval'+ttm[2]).val(numbs[2]);
                            }
                        } else if(troopshome[1]>0) {                            
                            numbs[2]=Math.ceil(ratio[1]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[1]]);
                            if(Number(troopshome[1])>=numbs[2]) {
                                $('#rval'+ttm[1]).val(numbs[2]);
                                $('#wcraidunti').val(1);
                                $('#inprettimewc').hide();
                            } else {
                                message="Impossible action, one needs more troops at least " + numbs[2] + " " + ttname[ttm[1]]+"!";
                                errorgo(message);
                                $('#wcraidunti').val(3);
                                $('#inprettimewc').show();
                            }
                            if((Number(troopshome[1])/Number(numbs[2]))<count) {
                                count=Number(troopshome[1])/Number(numbs[2]);
                            }
                            numbs[2]=Math.ceil(ratio[2]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[2]]);
                            if(Number(troopshome[1])>=numbs[2]) {
                                $('#rval'+ttm[2]).val(numbs[2]);
                            }
                        } else {
                            numbs[2]=Math.ceil(ratio[2]*loot[numbs[0]]*((100-numbs[1])*0.008+1)/ttloot[ttm[2]]);
                            if(Number(troopshome[2])>=numbs[2]) {
                                $('#rval'+ttm[2]).val(numbs[2]);
                                $('#wcraidunti').val(1);
                                $('#inprettimewc').hide();
                            } else {
                                message="Impossible action, one needs more troops at least " + numbs[2] + " " + ttname[ttm[2]]+"!";
                                errorgo(message);
                                $('#wcraidunti').val(3);
                                $('#inprettimewc').show();
                            }
                            if((Number(troopshome[2])/Number(numbs[2]))<count) {
                                count=Number(troopshome[2])/Number(numbs[2]);
                            }
                        }
                            
                    }
                }
                //console.log("count:"+count);
                count=Math.floor(count);
                setTimeout(function(){$('.splitRaid').val(count);}, 500);
        }, 300);
        });
        });
    }
    // generate buildings count for city
function makebuildcount() {
    $("#bdtable").remove();
    var currentbd={name:[],bid:[],count:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]};
    var j;
    var bdtypecount=-1;
    var bdNumber=-1;


    for (var i in cdata.bd) {
        if (buildings.bid.indexOf(cdata.bd[i].bid)>-1) {
            if (currentbd.bid.indexOf(cdata.bd[i].bid)>-1) {
                j=currentbd.bid.indexOf(cdata.bd[i].bid);
                currentbd.count[j]+=1;
                bdNumber+=1;
            } else {
                bdtypecount+=1;
                j=buildings.bid.indexOf(cdata.bd[i].bid);
                currentbd.name[bdtypecount]=buildings.name[j];
                currentbd.bid[bdtypecount]=buildings.bid[j];
                currentbd.count[bdtypecount]+=1;
                bdNumber+=1;
            }
        }
    }
    //console.log(currentbd);
    var bdtable="<table id='bdtable'><tbody><tr>";
    for (var i in currentbd.bid) {
        if (i<9 || ((i>9 && i<19) || (i>19 && i<29))) {
            bdtable+="<td style='text-align:center; width:32px; height:32px;'><div style='background-image: url(/images/city/buildings/icons/"+currentbd.name[i]+".png); background-size:contain;background-repeat:no-repeat;width:32px; height:32px;'></div>"+Number(currentbd.count[i])+"</td>";
        }
        if (i==9 || i==19) {
            //console.log(i);
            bdtable+="</tr><tr>";
            bdtable+="<td style='text-align:center; width:32px; height:32px;'><div style='background-image: url(/images/city/buildings/icons/"+currentbd.name[i]+".png); background-size:contain;background-repeat:no-repeat;width:32px; height:32px;'></div>"+Number(currentbd.count[i])+"</td>";
        }
    }
    bdtable+="</tr></tbody></table>";
    $("#bdcountwin").html(bdtable);
    $("#numbdleft").html(bdNumber);
}
    var bdcountshow=true;
    $(document).ready(function() {
        var bdcountbox="<div id='currentBd'><div id='bdcountbar' class='queueBar' style='border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;'>";
        bdcountbox+="<div id='bdcountbut' class='tradeqarr2'><div></div></div><span class='qbspan'>Current Buildings</span>";
        bdcountbox+="<div id='numbdleft' class='barRightFloat tooltipstered'>0</div></div><div id='bdcountwin' class='queueWindow' style='display: block;'></div></div>";
        $("#bqitemss").after(bdcountbox);
        $("#bdcountbar").click(function() {
            if (bdcountshow) {
                //console.log(1);
                $("#bdcountwin").hide();
                $("#bdcountbar").attr("style","border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;");
                $("#bdcountbut").removeClass('tradeqarr2').addClass('tradeqarr1');
                bdcountshow=false;
            } else {
                $("#bdcountwin").show();
                $("#bdcountbar").attr("style","border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;");
                $("#bdcountbut").removeClass('tradeqarr1').addClass('tradeqarr2');
                bdcountshow=true;
            }
        });
    });
})();










